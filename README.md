# Renovate Configuration

Common SLAGIT.net renovate configuration.

## gitlab>slagit/renovate-config

Basic default configuration for all SLAGIT.net projects.

* Assign MRs to akoch
* Enable dependency dashboard, autoclose if no outstanding updates
* Limit to four updates per hour

## gitlab>slagit/renovate-config:golang

Configuration extension for golang projects.

* Go module updates are "fix(deps)" rather than "chore(deps)"
* Run `go mod tidy` after each update
